import { Message as VercelChatMessage } from 'ai';
import { CallbackHandler } from "langfuse-langchain";
import { ChatOpenAI } from "@langchain/openai";
import { ChatPromptTemplate } from "@langchain/core/prompts";
import { RecursiveCharacterTextSplitter } from "langchain/text_splitter";
import { OpenAIEmbeddings } from "@langchain/openai";
import { MemoryVectorStore } from "langchain/vectorstores/memory";
import { createStuffDocumentsChain } from "langchain/chains/combine_documents";
import { createRetrievalChain } from "langchain/chains/retrieval";
import { Document } from "langchain/document";
import { createHistoryAwareRetriever } from "langchain/chains/history_aware_retriever";
import { MessagesPlaceholder } from "@langchain/core/prompts";
import { AIMessage, HumanMessage } from 'langchain/schema';
import { createRetrieverTool } from "langchain/tools/retriever";
import { createOpenAIFunctionsAgent, AgentExecutor } from "langchain/agents";


const data = `---
layout: cv
title: James Martin
---

# James Martin

## Fullstack/Product Engineer

**Location:** France (seeking for a remote position)  
**Email:** contact@james-martin.dev  
**Website:** [james-martin.dev](https://www.james-martin.dev)  
**LinkedIn:** [linkedin.com/in/dorkside](https://www.linkedin.com/in/dorkside)

## Objective

Innovative and adaptable Fullstack Engineer with a strong foundation in the JS/TS ecosystem, special interest in AI, and experience in product engineering. Eager to contribute to a meaningful project with impact that values fast iteration and collaboration.
Specially good at leading fast-paced projects, and building creative solutions to complex problems.

## Professional Experience

### Technical lead | AI Engineer (2019 - Present)

#### _AXA Group Operations, Lausanne, Vaud, Switzerland_

##### AI Engineer (2023 - Present)

- Specialized in the industrialization of AI technologies, with a focus on enhancing user experience and product functionality. Langchain, Llamaindex, OpenAI, MLFlow and Airflow have become a part of my toolbox.
- Proficient in collaborating with cross-functional teams, aligning AI project goals with broader business objectives.

##### Technical Lead (2021 - Present)

- Expertise in the JS/TS ecosystem, leading to the successful deployment of scalable software solutions.
- Implemented process optimizations, significantly improving project execution and efficiency.

##### Software Engineer (2019 - 2021)

- Developed a customer-facing widget using JS, resulting in improved performance and user experience, while reducing training costs for call center agents.
- Adapted to emerging technologies to enhance product features.

### Technical lead (2015-2019)

#### _SPARTE, Lyon, France_

- Managed a team to develop a BIM data management platform, leveraging Noade, GraphQL and Angular.
- Demonstrated strong leadership in coordinating multidisciplinary teams.

### Full-Stack Developer (2015)

#### _Créati'ON Studio, Lyon, France_

- Designed a collaborative maintenance tool in Node, Firebase and Ionic, showcasing innovation in software solution development.

### Founding engineer (2014-2015)

#### _InstanT, Lyon, France_

- Designed and developed a fully functional uber-like platform for interpreters based on Ruby on Rails and Android.

## Education

**Cnam** (Expected Graduation June 2025)

- Engineering degree, Computer Science

**Lycée la Martinière Duchère** (2013 - 2015)

- BTS SIO, Computer science

## Skills

- **Technical skills:** Proficient in the JS/TS ecosystem : Node.js, GraphQL, Firebase, Vue, Angular, React; Familiar with Ruby on Rails and Python; Experience with AI technologies.
- **Soft Skills:** Proven leadership, team management, effective communication, and stakeholder engagement.
- **Languages:** English (Bilingual), French (Bilingual), Italian (Intermediate)

## References

Available upon request
`;

export default defineLazyEventHandler(async () => {
    const {
        langfuseSecretKey,
        langfusePublicKey,
    } = useRuntimeConfig();

    if (!langfuseSecretKey || !langfusePublicKey) {
        throw new Error("Missing Langfuse API keys");
    }

    const handler = new CallbackHandler({
        secretKey: langfuseSecretKey,
        publicKey: langfusePublicKey,
    });

    const docs = [new Document({ pageContent: data })]

    const splitter = new RecursiveCharacterTextSplitter();

    const splitDocs = await splitter.splitDocuments(docs);
    const embeddings = new OpenAIEmbeddings();

    const vectorstore = await MemoryVectorStore.fromDocuments(
        splitDocs,
        embeddings
    );

    const prompt = ChatPromptTemplate.fromMessages([
        ["system", "Your are a software engineer, called James D. Martin. You are talking to visitors on your personal website [james-martin.dev]. Always try to provide short but helpful answers, inviting for more details if needed. Be polite and professional. Be subtle about your openness to new opportunities."],
        new MessagesPlaceholder("chat_history"),
        ["human", "{input}"],
        new MessagesPlaceholder("agent_scratchpad"),
    ]);

    const retriever = vectorstore.asRetriever();

    const retrieverTool = await createRetrieverTool(retriever, {
        name: "james_search",
        description:
            "Search for information about James. For any questions about James' CV, you must use this tool!",
    });

    const tools = [retrieverTool];

    const agentModel = new ChatOpenAI({
        modelName: "gpt-3.5-turbo-1106",
        temperature: 0,
        streaming: true
    });

    const agent = await createOpenAIFunctionsAgent({
        llm: agentModel,
        tools,
        prompt,
    });

    const agentExecutor = new AgentExecutor({
        agent,
        tools,
        verbose: true,
    });

    return defineEventHandler(async event => {
        const { messages } = (await readBody(event)) as {
            messages: Array<VercelChatMessage>
        };

        const chat_history = (messages as VercelChatMessage[]).map(m =>
            m.role == 'user'
                ? new HumanMessage(m.content)
                : new AIMessage(m.content),
        )

        const input = chat_history[chat_history.length - 1].content as string;

        const response = await agentExecutor.invoke({ input, chat_history }, { callbacks: [handler] })
        return response.output;
    });
});